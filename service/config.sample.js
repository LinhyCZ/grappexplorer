var config = {};

config.https = require('https');
config.fs = require('fs');

config.express = require('express');
config.app = config.express();

config.httpsServerCredentials = {
  cert: config.fs.readFileSync("crt.pem"),
  key: config.fs.readFileSync("key.pem"),
  ca: config.fs.readFileSync('chain.pem'),
  requestCert: false,
  rejectUnauthorized: false
}


config.httpsServer = config.https.createServer(config.httpsServerCredentials, config.app);

config.publicVAPIDkey = "PublicVAPID";
config.privateVAPIDkey = "PrivateVAPID";

config.accessKey = null;


config.getAllTrainsFilterData = '{"CarrierCode":["991919","992230","992719","993030","990010","993188","991943","991950","991075","993196","992693","993147","991638","991976","993089","993162","991257","991935","991562","991125","992644","992842","991927","993170","991810","992909","991612"],"PublicKindOfTrain":["LE","Ex","Sp","rj","TL","EC","SC","AEx","Os","Rx","TLX","IC","EN","R","RJ","nj","LET"],"FreightKindOfTrain":[],"TrainRunning":false,"TrainNoChange":0,"TrainOutOfOrder":false,"Delay":["0","60","5","61","15","-1","30"],"DelayMin":-99999,"DelayMax":-99999,"SearchByTrainNumber":true,"SearchExtraTrain":false,"SearchByTrainName":true,"SearchByTRID":false,"SearchByVehicleNumber":false,"SearchTextType":"0","SearchPhrase":"","SelectedTrain":-1}';

config.getAllTrainsInfoOptions = {
    host: 'grapp.spravazeleznic.cz',
    path: '/post/trains/GetTrainsWithFilter/{1}',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': config.getAllTrainsFilterData.length
    }
}

config.getOneTrainBasicInfoOptions = {
    host: 'grapp.spravazeleznic.cz',
    path: '/OneTrain/MainInfo/{1}?trainId={2}'
}

config.getOneTrainRouteOptions = {
    host: 'grapp.spravazeleznic.cz',
    path: '/OneTrain/RouteInfo/{1}?trainId={2}'
}

config.getOneTrainGPSCoordsOptions = {
    host: 'grapp.spravazeleznic.cz',
    path: '/get/trains/train/{1}?trainId={2}'
}


config.homePageGetOptions = {
    host: 'grapp.spravazeleznic.cz',
    path: ''
}

module.exports = config;
