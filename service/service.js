const config = require('./config.js');

const webPush = require('web-push');
const bodyParser = require('body-parser');
const _ = require('lodash');

var previousDifferentTrains = [];
var subscriptions = [];

var differentTrains;


loadSubscriptions();

launchSocketServer(assignNodeEvents);
launchWebApiServer();

getPageAccessKey(function() {
  proccessDifferentTrains();
})

startDifferentTrainCheckInterval();



/**
 * FUCNTIONS
 */
function proccessDifferentTrains() {
  getAllTrainsJsonData(function(jsonObject) {
    differentTrains = filterKnownTrains(jsonObject);

    emitDifferentTrains();
    prepareDifferentTrainsMessage();
  });
}

function startDifferentTrainCheckInterval() {
  config.checkInterval = setInterval(proccessDifferentTrains, 60 * 1000);
}

function launchWebApiServer() {
  webPush.setVapidDetails("mailto:admin@linhy.cz", config.publicVAPIDkey, config.privateVAPIDkey);

  config.app.use(bodyParser.json());
  config.app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    return next();
  });

  handleExpressEvents();
}

function handleExpressEvents() {
  config.app.post('/subscribe', (req, res) => {
    const body = req.body;
    let sendMessage;

    let pushSubscription = true;

    for (let i = 0; i < subscriptions.length; i++) {
      if (subscriptions[i].subscription.endpoint == body.subscription.endpoint) {
        subscriptions[i]["latitude"] = body.latitude;
        subscriptions[i]["longtitude"] = body.longtitude;
        subscriptions[i]["radius"] = body.radius;
        subscriptions[i]["unsubscribe"] = body.unsubscribe;

        pushSubscription = false;
        sendMessage = 'Subscription stored';
        break;
      }
    }

    if (pushSubscription) {
      subscriptions.push(body);

      sendMessage = 'Subscription stored';
    }

    storeSubscriptions();
    res.send(sendMessage);
  });
}

function storeSubscriptions() {
  config.fs.writeFile("subscriptions.json", JSON.stringify(subscriptions), function(err) {
    if (err) throw err;
  });
}

function loadSubscriptions() {
  if (config.fs.existsSync('subscriptions.json')) {
    subscriptions = JSON.parse(config.fs.readFileSync('subscriptions.json', 'utf8'));
  } else {
    subscriptions = [];
  }
}

function sendNewTrainToEverybody(message) {
  console.log("Parsing subscription for train: " + message["Title"]);
  if (subscriptions.length) {
    subscriptions.map((subscription, index) => {
      if (subscription.unsubscribe === false) {
        let emit = false;
        if (subscription.radius !== "null") {
          console.log("Calculating distances!");
          for (let i = 0; i < message["GPSpath"].length; i++) {
            distance = distanceInKmBetweenEarthCoordinates(subscription.latitude, subscription.longtitude, Number(message["GPSpath"][i][0]), Number(message["GPSpath"][i][1]));

            if (distance <= subscription.radius) {
              emit = true;
              break;
            }
          }
        } else {
          console.log("Radius is null, sending every notification");
          emit = true;
        }

        if (emit) {
          console.log("Emitting!");
          let jsonSub = subscription.subscription;

          // Use the web-push library to send the notification message to subscribers
          webPush
          .sendNotification(jsonSub, JSON.stringify(message))
          .then(success => handleSuccessNotification(success, index))
          .catch(error => handleErrorNotification(error, index));
        }
      }
    });
  }
}

function handleSuccessNotification(success, index) {
  console.log('Push notification published successfully!');
}

function handleErrorNotification(error, index) {
  console.log('Some or all notifications failed to be published');
}


function launchSocketServer(callback) {
  config.httpsServer.listen(4568);

  config.io = require('socket.io').listen(config.httpsServer);
  config.io.on("connection", function(socket) {
    callback(socket);
  });
}

function assignNodeEvents(socket) {
  socket.on("getOneTrainBasicInfo", function(data) {messageGetOneTrainBasicInfo(data, socket)});
  socket.on("getOneTrainRoute", function(data) {messageGetOneTrainRoute(data, socket)});
  socket.on("getCompleteData", function(data) {messageGetCompleteData(data, socket)});
  socket.on("getMoreInfoUnknown", function(data) {messageGetMoreInfoUnknown(data, socket)})
  socket.on("getMoreInfoDifferent", function(data) {messageGetMoreInfoDifferent(data, socket)})
}

function messageGetOneTrainBasicInfo(data, socket) {
  getOneTrainBasicInfo(data["trainId"], function(data) {
    emitBasicInfo(socket, extractBasicInfo(data));
  });
}

function messageGetOneTrainRoute(data, socket) {
  getOneTrainRoute(data["trainId"], function(data) {
    emitRoute(socket, extractRoute(data));
  });
}

function messageGetCompleteData(data, socket) {
  getCompleteData(function(data) {
    emitCompleteData(socket, data);
  })
}

async function messageGetMoreInfoUnknown(data, socket) {
  trainID = JSON.parse(data)["trainId"];
  trainRouteRaw = await getOneTrainRoutePromise(trainID);
  trainRoute = JSON.parse(extractRoute(trainRouteRaw));

  toReturn = {"trainId": trainID, "trainRoute": trainRoute};

  emitMoreInfoUnknown(socket, JSON.stringify(toReturn));
}

async function messageGetMoreInfoDifferent(data, socket) {
  trainID = JSON.parse(data)["trainId"];
  basicInfoRaw = await getOneTrainBasicInfoPromise(trainID);
  basicInfo = JSON.parse(extractBasicInfo(basicInfoRaw));
  trainRouteRaw = await getOneTrainRoutePromise(trainID);
  trainRoute = JSON.parse(extractRoute(trainRouteRaw));

  let toReturn = {"trainId": trainID, "basicInfo": basicInfo, "trainRoute": trainRoute};

  emitMoreInfoDifferent(socket, JSON.stringify(toReturn));
}

function emitDifferentTrains() {
  config.io.emit("newDifferentTrains", JSON.stringify(differentTrains));
}

function emitMoreInfoUnknown(socket, data) {
  emitMessage(socket, "moreInfoUnknown", data);
}

function emitMoreInfoDifferent(socket, data) {
  emitMessage(socket, "moreInfoDifferent", data);
}

function distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2) {
  let p = 0.017453292519943295;    // Math.PI / 180
  let c = Math.cos;
  let a = 0.5 - c((lat2 - lat1) * p)/2 +
          c(lat1 * p) * c(lat2 * p) *
          (1 - c((lon2 - lon1) * p))/2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

function prepareDifferentTrainsMessage() {
  let newDifferentTrains = _.filter(differentTrains["unknownTrain"], (e) => !previousDifferentTrains.includes(e.Title));

  console.log("New diff trains length: " + newDifferentTrains.length);

  parseNewDifferentTrains(newDifferentTrains);
  //parseNewDifferentTrains(differentTrains["unknownTrain"]);
  previousDifferentTrains = [];
  differentTrains["unknownTrain"].forEach(function(item, index) {
    previousDifferentTrains.push(item.Title);
  });
}

async function parseNewDifferentTrains(newDifferentTrains) {
  console.log("Parsing new different trains. Number of trains: " + newDifferentTrains.length);
  for (let differentTrainIteration = 0; differentTrainIteration < newDifferentTrains.length; differentTrainIteration++) {
    console.log("Parsing train number: " + differentTrainIteration);
    currentTrain = newDifferentTrains[differentTrainIteration];

    currentTrainGPSRaw = await getOneTrainGPSCoordsPromise(currentTrain["Id"]);
    currentTrainGPS = extractGPSCoords(currentTrainGPSRaw);
    currentTrainBasicInfoRaw = await getOneTrainBasicInfoPromise(currentTrain["Id"]);
    currentTrainBasicInfo = extractBasicInfo(currentTrainBasicInfoRaw);

    currentTrain.basicInfo = JSON.parse(currentTrainBasicInfo);
    currentTrain.GPSpath = currentTrainGPS;

    console.log("Calling prepare for train number: " + differentTrainIteration);
    prepareMessage(currentTrain);
  }
}

function extractGPSCoords(rawData) {
  objectToReturn = [];
  jsonData = JSON.parse(rawData);

  for (let i = 0; i < jsonData["Confirmed1"].length; i++) {
    objectToReturn.push(jsonData["Confirmed1"][i]);
  }

  for (let i = 0; i < jsonData["InPlan1"].length; i++) {
    objectToReturn.push(jsonData["InPlan1"][i]);
  }

  return objectToReturn;
}

function prepareMessage(train) {
  console.log("New train message " + new Date());
  sendNewTrainToEverybody(train);
}

function filterKnownTrains(jsonObject) {
  let newObject = {"differentCategory": [], "unknownTrain": []};
  let plannedTrainNames = JSON.parse(config.fs.readFileSync('knownTrains.json', 'utf8'));
  let trainNumbers = Object.keys(plannedTrainNames);
  let trainNames = JSON.parse(config.fs.readFileSync('knownTrainsNames.json', 'utf8'));


  for (let i = 0; i < jsonObject["Trains"].length; i++) {
    let currentTrain = jsonObject["Trains"][i];
    let nameOfTrainSplit = currentTrain["Title"].trim().split(" ");
    let nameOfTrain = nameOfTrainSplit[0] + " " + nameOfTrainSplit[1];
    let numberOfTrain = nameOfTrainSplit[1];

    if(!trainNames.includes(nameOfTrain)) {
      let plannedName = plannedTrainNames[numberOfTrain] + " " + numberOfTrain;
      if (trainNumbers.includes(numberOfTrain)) {
        //Neobvyklé řazení, např IC -> SP
        currentTrain.plannedName = plannedName;
        newObject["differentCategory"].push(currentTrain);
      } else {
        newObject["unknownTrain"].push(currentTrain);
      }
    }
  }

  return newObject;
}

function getAllTrainsJsonData(callback) {
  let postData = '';
  let parsedJson;

  let requestOptions = duplicate(config.getAllTrainsInfoOptions);

  requestOptions.path = requestOptions.path.replace("{1}", config.accessKey);

  const allTrainsRequest = config.https.request(requestOptions, (res) => {

    res.on('data', (chunk) => {
      postData += chunk;
    })

    res.on('end', function () {
      parsedJson = JSON.parse(postData);
      validateAccessKey(getAllTrainsJsonData, callback, parsedJson);
    });
  })

  allTrainsRequest.on('error', (error) => {
    console.error(error)
  })

  allTrainsRequest.write(config.getAllTrainsFilterData)
  allTrainsRequest.end()
}

function validateAccessKey(checkFunction, callback, jsonToValidate) {
  if (Object.keys(jsonToValidate["Trains"]).length == 0) {
    getPageAccessKey(function() {
      checkFunction(callback);
    })
  } else {
    callback(jsonToValidate);
  }
}

function getPageAccessKey(callback) {
  getHomePageContent(function(pageData) {
    let $ = getJqueryFromHtml(pageData);
    config.accessKey = $("#token").val();
    callback();
  });
}

function getJqueryFromHtml(pageData) {
  const jsdom = require("jsdom");
  const { JSDOM } = jsdom;
  const { window }  = new JSDOM(pageData);

  return require("jquery")(window);
}

function getHomePageContent(callback) {
  makeGETRequest(config.homePageGetOptions, callback);
}

function emitMessage(socket, key, data) {
  socket.emit(key, data);
}

function getOneTrainBasicInfo(trainId, callback) {
  let requestOptions = duplicate(config.getOneTrainBasicInfoOptions);

  requestOptions.path = requestOptions.path.replace("{1}", config.accessKey).replace("{2}", trainId);
  makeGETRequest(requestOptions, callback);
}

function getOneTrainRoute(trainId, callback) {
  let requestOptions = duplicate(config.getOneTrainRouteOptions);

  requestOptions.path = requestOptions.path.replace("{1}", config.accessKey).replace("{2}", trainId);
  makeGETRequest(requestOptions, callback);
}

function getOneTrainGPSCoords(trainId, callback) {
  let requestOptions = duplicate(config.getOneTrainGPSCoordsOptions);

  requestOptions.path = requestOptions.path.replace("{1}", config.accessKey).replace("{2}", trainId);
  makeGETRequest(requestOptions, callback);
}

function makeGETRequest(options, callback) {
  let getRequest = config.https.request(options, function (res) {
    let data = '';
    res.on('data', function (chunk) {
        data += chunk;
    });
    res.on('end', function () {
      callback(data);
    });
  });


  getRequest.on('error', function (e) {
      console.log("Error:" + e.message);
  });
  getRequest.end();
}

function emitBasicInfo(socket, data) {
  emitMessage(socket, "oneTrainBasicInfo", data);
}

function emitRoute(socket, data) {
  emitMessage(socket, "oneTrainRoute", data);
}

function emitCompleteData(socket, data) {
  emitMessage(socket, "completeData", data);
}

function getOneTrainGPSCoordsPromise(trainId) {
  return new Promise(function (resolve, reject) {
    getOneTrainGPSCoords(trainId, function(data) {
      resolve(data);
    })
  })
}

function getOneTrainBasicInfoPromise(trainId) {
  return new Promise(function (resolve, reject) {
    getOneTrainBasicInfo(trainId, function(data) {
      resolve(data);
    })
  })
}

function getOneTrainRoutePromise(trainId) {
  return new Promise(function (resolve, reject) {
    getOneTrainRoute(trainId, function(data) {
      resolve(data);
    })
  })
}

function getAllTrainsJsonDataPromise() {
  return new Promise(function (resolve, reject) {
    getAllTrainsJsonData(function(jsonObject) {
      resolve(jsonObject);
    })
  })
}

function compare(a,b) {
  if (a["Id"] > b["Id"]) {
    return 1;
  } else {
    return -1;
  }

  return 0;
}

async function getCompleteData(callback) {
  newJsonObject = {};

  jsonObject = await getAllTrainsJsonDataPromise();
  filteredTrains = filterKnownTrains(jsonObject);

  newJsonObject["differentCategory"] = JSON.parse(JSON.stringify(filteredTrains["differentCategory"]));

  filteredTrains["unknownTrain"].sort(compare);

  unknownTrains = JSON.parse(JSON.stringify(filteredTrains["unknownTrain"]));
  console.log("Unknown trains count: " + unknownTrains.length);

  for (let trainIteration = 0; trainIteration < unknownTrains.length; trainIteration++) {
    currentTrainBasicInfoRaw = await getOneTrainBasicInfoPromise(unknownTrains[trainIteration]["Id"]);
    currentTrainBasicInfo = extractBasicInfo(currentTrainBasicInfoRaw);

    /** TODO - Upravit na klonování objektu? Nejdřív klon differentCategory a pak projít nový objekt unknownTrains a přidat do nového objektu */;
    unknownTrains[trainIteration].basicInfo = JSON.parse(currentTrainBasicInfo);
  }

  newJsonObject["unknownTrain"] = unknownTrains;
  callback(newJsonObject);
}

function extractBasicInfo(data) {
  let newObject = {};
  let $ = getJqueryFromHtml(data);
  if ($(".alert-warning").length != 0) {
    //Information unknown
    newObject.error = "Data out of date";
    return JSON.stringify(newObject);
  } else {
    newObject.trainNumber = $(".row:nth-child(1) div").html().trim();
    newObject.carrier = $(".row:nth-child(2) div a").html().trim();
    newObject.startStation = $(".row:nth-child(4) div:last-child").html().trim();
    newObject.finishStation = $(".row:nth-child(5) div:last-child").html().trim();
    newObject.currentStation = $(".row:nth-child(6) div:last-child").html().trim();
    newObject.estimatedTime = $(".row:nth-child(8) div:last-child").html().trim();
    newObject.realTime = $(".row:nth-child(9) div:last-child").html().trim();
    newObject.delay = verifyNaN(Number($(".row:nth-child(11) div:last-child").html().trim().replace("min", "")));

    if ($(".row:nth-child(11) div:first-child").html().trim() == "náskok") {
      newObject.delay = newObject.delay * -1;
    }

    if ($(".row:nth-child(13) div:last-child").length == 0) {
      newObject.expectedDelay = newObject.delay;
      newObject.state = "finished";
    } else {
      newObject.expectedDelay = verifyNaN(Number($(".row:nth-child(13) div:last-child").html().trim().replace("min", "")));
      $(".row:nth-child(13) div:first-child").children("sup").remove(); //remove help button
      if ($(".row:nth-child(13) div:first-child").html().trim() == "předpokládaný náskok") {
        newObject.expectedDelay *= -1;
      }
      if($(".row:nth-child(8) div:first-child").html().trim().split(" ")[1] == "příjezd") {
        newObject.state = "arrived";
      } else {
        newObject.state = "departed";
      }
    }




    return JSON.stringify(newObject);
  }
}

function extractRoute(data) {
  let newObject = {};
  let $ = getJqueryFromHtml(data);

  let rootElement = $(".route .row");

  for (let i = 0; i < rootElement.length; i++) {
    inStation = false;
    let currentNode = $(rootElement[i]);
    let currentStation = currentNode.children("div:first-child");
    if ($(currentStation).children("a").length > 0) {
      let currentStationElement = $(currentStation).children("a:first-child");
      if (currentStationElement.children("svg").length > 0) {
        currentStationElement.children("svg").remove();
        inStation = true;
      }
      currentStationName = currentStationElement.html().trim();
    } else if ($(currentStation).children("svg").length > 0) {
      currentStationName = $(currentStation).children("span").html().trim();
      inStation = true;
    } else {
      currentStationName = $(currentStation).html().trim();
    }

    if (currentStationName != "Informace o vlaku") {
      newObject[currentStationName] = {};

      if (i != 0) {
        if (rootElement[0] == undefined) {
          arrivalTimeElement = $(rootElement).find(".hidden-lg:last-child");
        } else {
          arrivalTimeElement = $($(rootElement[i]).children(".hidden-lg")[1]);
        }

        realArivalTime = arrivalTimeElement.children("span:first-child").html().trim();
        plannedArivalTime = arrivalTimeElement.children("span:last-child").html().trim().replace("(", "").replace(")", "");

        if (i == rootElement.length - 2) {
          realDepartureTime = null;
          plannedDepartureTime = null;
        } else {
          departureTimeElement = arrivalTimeElement.next().children(".hidden-lg");
          realDepartureTime = departureTimeElement.children("span:first-child").html().trim();
          plannedDepartureTime = departureTimeElement.children("span:last-child").html().trim().replace("(", "").replace(")", "");
        }
      } else {
        realArivalTime = null;
        plannedArivalTime = null;

        departureTimeElement = $(rootElement[0]).children("div:last-child").children("div:last-child").prev().children(".hidden-lg");
        realDepartureTime = departureTimeElement.children("span:first-child").html().trim();
        plannedDepartureTime = departureTimeElement.children("span:last-child").html().trim().replace("(", "").replace(")", "");
      }

      newObject[currentStationName].plannedArivalTime = plannedArivalTime;
      newObject[currentStationName].plannedDepartureTime = plannedDepartureTime;
      newObject[currentStationName].realDepartureTime = realDepartureTime;
      newObject[currentStationName].realArivalTime = realArivalTime;
      newObject[currentStationName].inStation = inStation;
    }
  }

  return JSON.stringify(newObject);
}

function verifyNaN(number) {
  if (isNaN(number)) {
    return 0;
  } else {
    return number;
  }
}

function duplicate(object) {
  return JSON.parse(JSON.stringify(object));
}
