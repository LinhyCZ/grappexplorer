var socket;
var tableBody;
var timer;

var GETparamInfo = undefined;

var timerInterval = 60;
var timerValue = timerInterval;

var latitude;
var longtitude;
var radius;

var isSubscribed = false;
var swRegistration = null;

function initialize() {
  socket = io(serverIp);
  tableBody = $("#mainTableBody");

  readRadiusValues();

  assignNodeEvents();

  parseGETparams();

  socket.on('connect', function() {
    requestCompleteInfo();
    startTimer();
  });

  socket.on('disconnect', function() {
    stopTimer();
  })

  initializeMessaging();
}

function readRadiusValues() {
  latitude = Cookies.get('latitude');
  longtitude = Cookies.get('longtitude');
  radius = Cookies.get('radius');

  $("#coordinates").val(decodeURIComponent(Cookies.get("coordinates")));
  $("#radius").val(radius);
}

function parseGETparams() {
  params = window.location.search.substr(1);

  if (params.length > 0) {
    params = params.split("=");
    if (params[0] == "showInfo") {
      GETparamInfo = params[1];
    }
  }
}

function updateBtn() {
  if (Notification.permission === 'denied') {
    $("#subscribe").removeClass("btn-success").removeClass("btn-danger").addClass("btn-dark").attr("disabled", "disabled").html("Zasílání zpráv je zakázáno");
    //updateSubscriptionOnServer(null);
    return;
  }

  if (isSubscribed) {
    $("#subscribe").removeClass("btn-success").removeClass("btn-dark").addClass("btn-danger").html("Odhlásit se od odebírání");
  } else {
    $("#subscribe").removeClass("btn-dark").removeClass("btn-danger").addClass("btn-success").html("Přihlásit se k odebírání");
  }

  $("#subscribe").removeAttr("disabled");
}

function subscribe() {
  $("#subscribe").attr("disabled", "disabled");
  if (isSubscribed) {
    unsubscribeUser();
  } else {
    subscribeUser();
  }
}

function initializeUI() {
    // Set the initial subscription value
  swRegistration.pushManager.getSubscription()
  .then(subscription => {
    isSubscribed = (subscription !== null);

    updateSubscriptionOnServer(subscription, !isSubscribed);

    if (isSubscribed) {
      console.log('User IS subscribed.');
    } else {
      console.log('User is NOT subscribed.');
    }

    updateBtn();
  });
}

function unsubscribeUser() {
  swRegistration.pushManager.getSubscription()
  .then(subscription => {

    if (subscription) {
      console.log("unsubscribing now!");
      updateSubscriptionOnServer(subscription, true);
      isSubscribed = false;
      updateBtn();
      return subscription.unsubscribe();
    }
  })
  .catch(err => {
    console.log('Error unsubscribing', err);
  })
}

function subscribeUser() {
  const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
  swRegistration.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: applicationServerKey
  })
  .then(subscription => {
    console.log('User is subscribed:', subscription);

    updateSubscriptionOnServer(subscription, false);

    isSubscribed = true;

    updateBtn();
  })
  .catch(err => {
    if (Notification.permission === 'denied') {
      console.warn('Permission for notifications was denied');
    } else {
      console.error('Failed to subscribe the user: ', err);
    }
    updateBtn();
  });
}

function updateSubscriptionOnServer(subscription, unsubscribe, callback) {
  // Here's where you would send the subscription to the application server

  object = {
    "latitude": (latitude === undefined ? "null" : latitude),
    "longtitude": (longtitude === undefined ? "null" : longtitude),
    "radius": (radius === undefined ? "null" : radius),
    "subscription": subscription,
    "unsubscribe": unsubscribe
  };

  console.log("Sending subscription info: ");
  console.log(object);

  if (subscription) {
    var jsonSubscription = JSON.stringify(object);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', subscribeAddress, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onload = function() {
      if (callback !== undefined) {
        callback(this.responseText);
      }
    };

    xhr.send(jsonSubscription);
    }
}

function initializeMessaging() {
  if ('serviceWorker' in navigator) {
    console.log('Service Worker and Push is supported');

    navigator.serviceWorker.register('sw.js') //TODO EDIT
    .then(swReg => {
      console.log('Service Worker is registered', swReg);

      swRegistration = swReg;

      initializeUI();
    })
    .catch(err => {
      console.error('Service Worker Error', err);
    });
  } else {
    console.warn('Push messaging is not supported');
    $("#subscribe").removeClass("btn-success").addClass("btn-dark").attr("disabled", "disabled").html("Zasílání zpráv je deaktivováno");
  }

  if (!('Notification' in window)) {
    console.log('Notifications not supported in this browser');
    return;
  }

  Notification.requestPermission(status => {
    console.log('Notification permission status:', status);
  });
}

function assignNodeEvents() {
  socket.on("completeData", messageCompleteData);
  socket.on("moreInfoUnknown", messageMoreInfoUnknown);
  socket.on("moreInfoDifferent", messageMoreInfoDifferent)
}

function messageCompleteData(data) {
  parseCompleteData(data);
}

function messageMoreInfoUnknown(data) {
  data = JSON.parse(data);

  basicInfoRaw = $("#row" + data["trainId"]).find(".rawData").val();

  basicInfo = JSON.parse(basicInfoRaw);
  data["basicInfo"] = basicInfo;
  fillMoreInfoTable(data, "unknown");
}

function messageMoreInfoDifferent(data) {
  fillMoreInfoTable(JSON.parse(data), "different");
}

function fillMoreInfoTable(data, mode) {
  console.log(data["trainRoute"]);
  tableRow = $("#moreInfoRow");
  tableRow.addClass("infoTrain-" + data["trainId"] + "-" + mode);
  topPart = $('<div class="w-100"><div class="w-50 mx-auto templateOffset mt-2"><div class="infoRow"><h4 class="textLeft defaultStation"></h4><h4 class="textRight carrier"></h4></div><div class="infoRow"><h4 class="textLeft finishStation"></h4><h4 class="textRight timeOfEvent"></h4></div><div class="infoRow"><h4 class="textLeft currentStation"></h4><h4 class="textRight delay"></h4></div></div></div>');

  topPart.find(".defaultStation").html("Výchozí stanice: " + data["basicInfo"]["startStation"]);
  topPart.find(".carrier").html("Dopravce: " + data["basicInfo"]["carrier"]);
  topPart.find(".finishStation").html("Cílová stanice: " + data["basicInfo"]["finishStation"]);
  topPart.find(".timeOfEvent").html("Čas: " + data["basicInfo"]["realTime"]);

  if (data["basicInfo"]["state"] == "departed") {
    topPart.find(".currentStation").html("Aktuálně: odjezd " + data["basicInfo"]["currentStation"]);
  } else if (data["basicInfo"]["state"] == "finished"){
    topPart.find(".currentStation").html("Aktuálně: ukončeno " + data["basicInfo"]["currentStation"]);
  } else {
    topPart.find(".currentStation").html("Aktuálně: příjezd " + data["basicInfo"]["currentStation"]);
  }

  if (data["basicInfo"]["delay"] < 0) {
    topPart.find(".delay").html("Náskok: " + (Number(data["basicInfo"]["delay"]) * -1) + "min");
  } else {
    topPart.find(".delay").html("Zpoždění: " + data["basicInfo"]["delay"] + "min");
  }

  bottomPart = $('<div class="w-100 border-top"><div class="w-50 mx-auto templateOffset mt-2"><h3 class="text-center">Jízdní řád</h3><div class="infoRow border-bottom"><h4 class="textLeft">Stanice</h4><h4 class="textRight text-center" style="width: 170px">Odjezd</h4><h4 class="textRight text-center" style="width: 170px">Příjezd</h4></div></div></div>');
  var beenInStation = false;
  let keys = Object.keys(data["trainRoute"]);
  for (let i = 0; i < keys.length; i++) {
    let currentStation = keys[i];
    let currentRoute = data["trainRoute"][currentStation];

    console.log(currentRoute);
    let template = $('<div class="infoRow"><h4 class="textLeft stationName"><h4 class="textRight text-center departureTime" style="width: 170px"></h4></h4><h4 class="textRight text-center arrivalTime" style="width: 170px"></h4></div>')
    if (currentRoute["inStation"] == true) {
      template.addClass("orangeText");
      beenInStation = true;
    } else if (!beenInStation) {
      template.addClass("grayText");
    }
    template.find(".stationName").html(currentStation);
    if (currentRoute["realArivalTime"] != undefined) {
      template.find(".arrivalTime").html(currentRoute["realArivalTime"] + " (" + currentRoute["plannedArivalTime"] + ")");
    }

    if (currentRoute["realDepartureTime"] != undefined) {
      template.find(".departureTime").html(currentRoute["realDepartureTime"] + " (" + currentRoute["plannedDepartureTime"] + ")");
    }

    bottomPart.find(".infoRow:last-child").after(template);
  }

  tableRow.children("td").children().remove();
  tableRow.children("td").append(topPart);
  tableRow.children("td").append(bottomPart);
}

function showMoreInfoDialog(data, show) {
  var spinner = $('<div style="margin-bottom: 20px;" id="infoSpinner"><div id="LoadingInfoLoaderWrapper" class="loaderInfo"><div class="lds-ring"><div></div></div></div><div id="LoadingInfoTextWrapper" class="loaderInfo"><h3 id="LoadingInfoText" class="orangeText">Načítám informace o konkrétním vlaku..</h3></div></div>');

  if (show) {
    if ($(".triggered").length > 0) {
      let button = $(".triggered");
      $("#moreInfoRow").remove();
      button.css("transform", "rotate(180deg)");
      button.removeClass("triggered");
      id = button.parent().parent().children("td:first-child").html().trim();
      if (button.hasClass("differentTrain")) {
        button.attr("onclick", 'moreInfoDifferent(' + id + ')');
      } else {
        button.attr("onclick", 'moreInfoUnknown(' + id + ')');
      }
    }

    $("#row" + data + " button").css("transform", "rotate(0deg)");
    $("#row" + data + " button").attr("onclick", 'showMoreInfoDialog(' + data + ', false)');
    let tableRow = $("<tr></tr>").attr("id", "moreInfoRow").append($("<td></td>").attr("colspan", "10").append(spinner));
    $("#row" + data + " button").addClass("triggered");
    $("#row" + data).after(tableRow);
  } else {
    $("#moreInfoRow").remove();
    $("#row" + data + " button").css("transform", "rotate(180deg)");
    $("#row" + data + " button").removeClass("triggered");
    if ($("#row" + data + " button").hasClass("differentTrain")) {
      $("#row" + data + " button").attr("onclick", 'moreInfoDifferent(' + data + ')');
    } else {
      $("#row" + data + " button").attr("onclick", 'moreInfoUnknown(' + data + ')');
    }
  }
}


function updateRadius() {
  swRegistration.pushManager.getSubscription()
  .then(subscription => {
    if (subscription) {
      var coordinates = $("#coordinates").val().split(",");

      if (coordinates.length != 2) {
        alert("Souřadnice špatně zadány, zadejte souřadnice ve formátu <latitude>,<longtitude>");
        return;
      }
      //49.1522997N, 14.1699533E
      latitude = Number(coordinates[0].trim().replace("N", "").replace("E", "").replace("S", "").replace("W", ""));
      longtitude = Number(coordinates[1].trim().replace("N", "").replace("E", "").replace("S", "").replace("W", ""));
      radius = Number($("#radius").val().trim());

      if (isNaN(latitude) || isNaN(longtitude)) {
        alert("Souřadnice špatně zadány, zadejte souřadnice ve formátu <latitude>,<longtitude>");
        return;
      }

      if ($("#radius").val().trim() == 0 || isNaN(radius)) {
        alert("Radius špatně zadán, zadejte radius v km");
      }

      Cookies.set("coordinates", $("#coordinates").val(), {expires: 100*365});

      Cookies.set('latitude', latitude, {expires: 100*365});
      Cookies.set('longtitude', longtitude, {expires: 100*365});
      Cookies.set('radius', radius, {expires: 100*365});
      updateSubscriptionOnServer(subscription, !isSubscribed, function(message) {
        console.log(message);
        if (message == 'Subscription stored') {
          $("#radiusUpdate").html("Úspěšně aktualizováno");
          setTimeout(function() {
            $("#radiusUpdate").html("Aktualizovat radius")
          }, 5000);
        }
      });
    } else {
      alert("Je nutné být přihlášený k odběru pro změnu rádiusu!")
    }
  });
}

function moreInfoUnknown(data) {
  showMoreInfoDialog(data, true);
  socket.emit("getMoreInfoUnknown", '{"trainId": ' + data + '}');
}

function moreInfoDifferent(data) {
  showMoreInfoDialog(data, true);
  socket.emit("getMoreInfoDifferent", '{"trainId": ' + data + '}');
}

function parseCompleteData(data) {
  console.log("Got complete data " + new Date());
  if (!data instanceof Object) {
    data = JSON.parse(data);
  }

  if ($("#moreInfoRow").length > 0) {
    infoTrainId = $("#moreInfoRow").attr("class").split("-")[1];
    foundId = false;

    for (let i = 0; i < data["differentCategory"].length; i++) {
      if (data["differentCategory"][i]["Id"] == infoTrainId) {
        foundId = true;
        break;
      }
    }

    if (foundId == false) {
      for (let i = 0; i < data["unknownTrain"].length; i++) {
        if (data["unknownTrain"][i]["Id"] == infoTrainId) {
          foundId = true;
          break;
        }
      }
    }

    if (foundId) {
      mode = $("#moreInfoRow").attr("class").split("-")[2];

      var oldInfoRow = $("#moreInfoRow");

      if (mode == "different") {
        moreInfoDifferent(infoTrainId);
      } else {
        moreInfoUnknown(infoTrainId);
      }

      tableBody.children().remove();
      parseDifferentCategory(data["differentCategory"]);
      parseUnknownTrains(data["unknownTrain"]);


      $("#row" + infoTrainId + " button").css("transform", "rotate(0deg)");
      $("#row" + infoTrainId + " button").attr("onclick", 'showMoreInfoDialog(' + infoTrainId + ', false)');
      $("#row" + infoTrainId + " button").addClass("triggered");

      $("#row" + infoTrainId).after(oldInfoRow);


    } else {
      tableBody.children().remove();
      parseDifferentCategory(data["differentCategory"]);
      parseUnknownTrains(data["unknownTrain"]);
    }
  } else {
    tableBody.children().remove();
    parseDifferentCategory(data["differentCategory"]);
    parseUnknownTrains(data["unknownTrain"]);
  }

  $("#LoadingOverlay").css("display", "none");

  if (GETparamInfo !== undefined) {
    $("#row" + GETparamInfo + " button").click();
    GETparamInfo = undefined;
  }
}

function requestCompleteInfo() {
  console.log("Asking for complete data " + new Date());
  socket.emit("getCompleteData", "{}");
}

function parseDifferentCategory(data) {
  for (i = 0; i < data.length; i++) {
    currentTrain = data[i];

    newRow = $("<tr></tr>").attr("id", "row" + currentTrain["Id"]);
    trainId = getTD(currentTrain["Id"]);
    realName = getTD(currentTrain["Title"]);
    plannedName = getTD(currentTrain["plannedName"]);

    space = $("<td></td>").attr("colspan", "6");
    moreInfoButton = $("<td></td>").append($('<button class="btn btn-orange mr-3 differentTrain" type="button" onclick="moreInfoDifferent(' + currentTrain["Id"] + ')" style="transform: rotate(180deg)">^</button>'));

    newRow.append(trainId, realName, plannedName, space, moreInfoButton);
    tableBody.append(newRow);
  }
}

function getTD(text) {
  return $("<td></td>").text(text).addClass("align-middle");
}

function startTimer() {
  timer = setInterval(timerEvent, 1000);
}

function timerEvent() {
  if (timerValue == 0) {
    requestCompleteInfo();
    timerValue = timerInterval;
  } else {
    timerValue--;
  }

  $("#updateTimer").html("Aktualizace za " + timerValue + "s");
}

function forceUpdate() {
  console.log("Force requesting data " + new Date());
  timerValue = timerInterval;

  socket.emit("getCompleteData", "{}");
}

function parseUnknownTrains(data) {
  for (i = 0; i < data.length; i++) {
    currentTrain = data[i];

    newRow = $("<tr></tr>").attr("id", "row" + currentTrain["Id"]);
    json = $("<input type='text' value='" + JSON.stringify(currentTrain["basicInfo"]) + "' style='display: none' class='rawData'>");
    trainId = getTD(currentTrain["Id"]);
    realName = getTD(currentTrain["Title"]);
    plannedName = getTD(currentTrain["plannedName"]);
    if (currentTrain["basicInfo"] !== undefined) {
      carrier = getTD(currentTrain["basicInfo"]["carrier"]);
      startStation = getTD(currentTrain["basicInfo"]["startStation"]);
      finishStation = getTD(currentTrain["basicInfo"]["finishStation"]);

      if (currentTrain["basicInfo"]["state"] == "departed") {
        currentStation = getTD(currentTrain["basicInfo"]["currentStation"] + " =>");
      } else {
        currentStation = getTD("=> " + currentTrain["basicInfo"]["currentStation"]);
      }

      currentStationTime = getTD(currentTrain["basicInfo"]["realTime"]);
      delay = getTD(currentTrain["basicInfo"]["expectedDelay"]);
    } else {
      carrier = getTD("xxxxx");
      startStation = getTD("xxxxx");
      finishStation = getTD("xxxxx");
      currentStation = getTD("xxxxx");
      currentStationTime = getTD("xxxxx");
      delay = getTD("xxxxx");

    }

    moreInfoButton = $("<td></td>").append($('<button class="btn btn-orange mr-3 unknownTrain" type="button" onclick="moreInfoUnknown(' + currentTrain["Id"] + ')" style="transform: rotate(180deg)">^</button>'));

    newRow.append(trainId, realName, plannedName, carrier, startStation, finishStation, currentStation, currentStationTime, delay, moreInfoButton, json);
    tableBody.append(newRow);
  }
}

function stopTimer() {
  clearInterval(timer);
  timerValue = timerInterval;
}

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}
