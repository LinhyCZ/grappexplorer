self.addEventListener('notificationclose', event => {
  const notification = event.notification;
  const trainId = notification.data.trainId;

  console.log('Closed notification for train: ' + trainId);
});

self.addEventListener('notificationclick', event => {
  const notification = event.notification;
  const trainId = notification.data.trainId;
  const action = event.action;

  if (action === 'close') {
    notification.close();
  } else {
    clients.openWindow('/?showInfo=' + trainId);
    notification.close();
  }

  // TODO 5.3 - close all notifications when one is clicked

});

self.addEventListener('push', event => {
  let body;
  let jsonData = event.data.json();

  if (event.data) {
    body = generateBody(jsonData);
  } else {
    body = 'Default body';
  }

  const options = {
    body: body,
    vibrate: [100, 50, 100],
    data: {
      dateOfArrival: Date.now(),
      trainId: jsonData.Id
    },
    actions: [
      {action: 'explore', title: 'Zobrazit detaily.'},
      {action: 'close', title: 'Zavřít'}
    ]
  };

  event.waitUntil(
    self.registration.showNotification('Nový neznámý vlak!', options)
  );
});

function generateBody(json) {
  var toReturn = json.basicInfo.trainNumber + "\n";
  toReturn += json.basicInfo.carrier + "\n";
  toReturn += json.basicInfo.startStation + " => " + json.basicInfo.finishStation + "\n";
  toReturn += "Aktuálně: " + json.basicInfo.currentStation + " (" + json.basicInfo.realTime + ")" + "\n";

  return toReturn;
}
